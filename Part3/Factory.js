/**
 * @author sairaj
 * 15)
 */

function Car (basicFeatures)
{
	this.model = basicFeatures.model;
	this.doors = basicFeatures.doors || 4;
	this.mileage = basicFeatures.mileage;
	this.cost=basicFeatures.cost;
	
	this.getInfo= function () {
		return this.model + this.mileage + this.cost;
}

function CarFactory () {}

CarFactory.prototype.CarFeatures = Car;


