Part 4

19) An example of a language that uses classical inheritance would be Java or Python. In these
object oriented languages classes are used and can be subclassed to pass their attributes to 
new classes. The classes serve as blueprints for creating new objects. JavaScript is an example
of a language that uses prototypical inheritance since it does not contain classes in its native
syntax. We use functions in JavaScript to emulate the use of classes and can inherit attributes
from functions using prototypes of these functions.

20) An example of two design patterns that work well together would be a factory and a decorator
pattern. A factory pattern is used to create many different objects that have varying attributes
and are essentially derived from one main parent class/function. A decorator pattern can be used 
to add additional attributes/methods to any one of these objects. An example would be creating a
vehicle factory pattern to create vehicles of various types and models, such as cars, trucks, fords,
and BMWs. A decorator pattern could add specific features to these respective cars, trucks or models.


